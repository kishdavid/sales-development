# Vendition
    (Introductory Sidenotes:)
        James Nielsen - CEO. Reason: Lack of Sales Bootcamps on his way up
        Course (Itself): Self Paced, goal 5 days. Fundamentals, prep you to become SDR.
        Understanding of Tech Sales. Specif. Development. Building a Pipeline. Learn Software. How to Apply, Interview, & Earn Job
        *SDR - Sales Development Rep
    
SD Overview > SD Framework > Outbound Prospecting (Coldcalls/Lead Generation) > Workplace Professionalism > Apply, Interview, & Earn Job


# Day 1

# - Tech Overview & What is Sales?
    B2B - Businesses selling to B
    B2B - B selling to Consumers
    *Some companies are both
    Enterprise - Large Co. w/ >1000 people & multiple levels
    Midmarket - 100-999 people & few levels
    SMB - Small to Medium bus. w/ 2-99 people 
    Individuals - You & Me
    3 Macro Departments: 
        - Sales & Marketing: Marketing, Sales Development, Sales, Customer Success
        - Research & Development: Engineering, Product
        - General & Administrative: Finance/Accounting, Legal, HR
    
    Jobs in Sales & Marketing:
        - SDR: Entry Level JOB, Build Pipeline
            - Inbound or Outbound
        - Account Executive: Closes Deals
            - Hunter or Farmer
        - Field Sales: Meet w/ Clients Face2Face
            - Direct Sales or Indirect Sales
        - Sales Engineering: Technical Experts in Customer Facing Meetings
        - Sales Operations: Run Operations, Set Processes, & Develop Workflow for the Sales Team
    Sales Funnel:
        - Leads (People) > Marketing Qualifying Leads (Interested People) > Sales Qualifying Leads > Opportunity > Pipeline

# - Sales Development Overview
    SD turns MQLs into SQLs, in order to become Opportunities
    SD divided into Inbound & Outbound
        - Inbound: Takes leads from Marketing & Qualifies them via Content, Social Media, Email Newsletter, etc.
        - Outbound: Creates sales Opportunities from people without inbound interest via Outreaching Emails,  LinkedIn, Coldcalls, etc.
    SD Roles:
        - Filter Inbound Leads
        - Outbound Prospecting
        - Training to become AE
    SD Responsibilities:
        - Understand Product
        - Uncover Needs of Clients
        - Articulate Value Prop (of Our Solution)
        - Collect Feedback
    B-A-N-T Sales Qualifications:
        - Budget
        - Authority: Are you speaking with someone who has Authority? If not, don't pitch to them.
        - Need: Help them realize that need. "Without fufilling customer's needs, there can't be value creation"
        - Timeline: "Know when the solution is needed, set expectations with the account, and don't drag it out. The #1 mistake sales makes is working on deals too long
    SDR Dev Career Progression:
        Inbound SDR > Outbound SDR > Account Exec > Field Sales
        *Takes 12-24 months to go from SDR to AE

    *ACV - Annual Contract Value
    Companies typically experience shorter sales cycles when selling to Mid-market or SMB businesses companies compared to selling to enterprise.

# - Software Tools
       *Techstack, you could always ask what your company uses in a job interview
    Top 3 Must Haves:
        1. CRMS (Customer Relationship Management) i.e. SALESFORCE (#1 CRM in the Market), zendesk, pipedrive, etc
            I. Single Source of Truth
            II. Territory/Account Ownership
            III. Activity Management
            IV. Account Status
        2. Data Gathering & List Building i.e. SALESFORCE, LinkedIn, zoominfo, etc
        3. Sales Engagement & Prospecting i.e. OUTREACH, livechat, LinkedIn sales navigator, zenprospect, etc

# - SALESFORCE
    Object Categories in Salesforce:
        1. Leads
            Marketing Team pre-populates this. SD teams operate from Lead Object. Leads convert into Accounts, Contacts, Opportunities
        2. Contacts
            Individuals that are mediums for that company 
        3. Accounts
            Name of Business, tracks company interactions. *Contacts connected to Accounts
        4. Opportunities
            Opportunity to sell to them. *Reselling, Cross-selling, Upselling offers multiple opportunities per Account
            Common Opportunity Stages:
                - Discovery: NEED?
                - Evaluation: Account has given Verbal Indication for NEED.
                - Negotiation: Contract Negotiated, then Account gives Verbal Agreement to Sign Contract
                - Close Won or Close Lost?: Agreed on Sale OR Could Not Agree + No Immediate Opportunity for Business.
    Other Objects:
        - Campaigns
            Tradeshow, Coldcalling Blitz, etc
        - Tasks & Activities
            Logging Calls, follow-ups, alerts, etc
        - Reports & Dashboards
            Analytics


# Day 2

# - Sales Development Frameworks: 
# 1. SPUN
    DISCOVER: Why Buy Anything? (They have a Problem that Needs to be Solved)
        1. Situation: Ask Ask Ask Questions before figuring out Problem. Guide them into Identifying their Problems
        2. Problems
        3. Understanding: QUESTION, CLARIFY, then CONCERN. Clearly Understand the Situation before moving on
        4. Need: Understand both Implicit & Explicit Need

# 2. SAVR  
    EVALUATION: Why Buy From Us?
        1. Solution: What is our Solution & How does it Solve their Problem?
        2. Alternative: What's their Alt to using our Product? Go to our Competitors? Do it themselves?
        3. VALUE: *WHAT VALUE DO WE BRING TO THEIR ORGANIZATION?
        4. Roi & Metrics: What is the Impact of our Solution? What Numbers define Success?

# 3. STUD
    NEGOTIATION: Why Buy Now?
    (*This is not a linear, step-by-step process)
        - Stakeholders: Who is the Champion & Economic Buyer? WHO ELSE IS INVOLVED?
            Ask: "Have you ever purchased software before? And do wanna run this by anyone before you sign on the dotted line?"
        - Timeline: Dates & Deliverables for each milestone? Get buy-in from them, COMMIT TO THE TIMELINE
        - Urgency: WHY BUY NOW? What's the COMPELLING EVENT?
        for example: deal closes on June 30th, ask "What is the impact on their business if they sign July 1st instead of June 30th?" if they can't answer, there isn't really urgent compelling event
        - Decision-making

# - Ideal Customer Profile aka ICP
    Who is your Ideal Customer? Parameters
        - Industry
        - Size of Company
        - Revenue
        - Geography
        - Business Model
        - Organizational Structure
        - Values
        - Willingness of Change
    * Don't sell AC to eskimos
    Example: ICP of a CRM called AI CRM Corp, rival to Salesforce
        - Size of Company: Startups, 10> sales reps (They're unlikely to have a solution or a CRM. Likely haven't haven't bought from salesforce)
        - Geography: North American Companies (We're also in the U.S.)
        - Business Model: B2B Software companies (We, too, are a B2B software company, so they may be more likely to sign on)
        - Organizational Structure: Companies with Technical Founders

# - Buyer Persona - Representation of your ICP
    Demographics (Quantitative) *You can find most of this on their LinkdIn
        - Job Title
        - Education
        - Years of experience
        - Years at current role
    Psychographics (Qualitative) *Must speak with them to find out most of these
        - Personality
        - Values
        - Interests
        - Working Style
    Example: ICP of a CRM called AI CRM Corp, rival to Salesforce
        - Persona 1: Sales Directors
            - lead the sales team & would be the Buyer
        - Persona 2: Technical/Product-centric Founders & CEOs
            - Don't have sales backgrounds or worked for other small companies
            - Have not purchased/used Salesforce/any CRM before
            - Values innovation, customer support, consultative approach
            - Want to save money, currently running their business on spreadsheet
            - Will likely take the call from a fellow B2B startup founder

# - Campaigns & Math of Sales Funnel
    Outbound Prospecting: Specific & Targeted prospecting to find the right sales opportunities
    Campaign:
        1. Email + Call + Social Touches
        2. Have a plan
        3. Timing & Frequency
        4. Experiment - every company & situation has a different cadence
    Example 10 Businessday Call, Email, Social Campaign:
        - 1st Monday: Email, Call, Social
        - 1st Wednesday: Call, Email
        - 1st Thursday: Social
        - 2nd Tuesday: Call, Email, Social
        - 2nd Thursday: Call
        - 2nd Thursday: Email
    Math of Sales Funnel - What is the % of won deals per # call
    Example: 
        1000 Calls -> 100 Connects -> 80 Convos -> 40 Discovery Calls -> 20 Opportunities -> 7 Closed Won.
        Thus, 7 won deals per 1000 calls.

# - Math of Email Replies
    For every new Email you send, the probability of them replying increases.
    But if they ignore the first handfull of emails, they'll likely ignore the rest.
    *Law of Diminishing Returns: 8 emails is optimum
    If you're targeting Fortune 500 brand co (only 500), keep trying to contact EVEN AFTER 15 attempts
    But if an even greater target list (i.e. everyone on the planet), recommended is 3 touches

# - Inbound, Outbound, Warmbound leads (and how to deal with them)
    Inbound: Companies Reaching out to you
        1. Don't assume they know Anything
        2. Go through discovery process like any other meeting
        3. Get your questions answered before going into too much of what you do
    Outbound: Companies have never heard of you nor received info from you
        - You are SELLING the MEETING
    Warmbound: Inbetween, warmed up by marketing and kinda familiar w/ your company but not ready to buy
        1. Marketing considers all outbound "Warmbound", because everyone in ICP are receiving emails
        2. Give Marketing credit -> More Marketing budget -> More Leads -> More Sales

# - Objection Handling
    *Important! Often is the difference between winning & losing a deal
    Common Objections:
        1. Bad timing
        2. We already have a solution
        3. "Send me Information"
        4. Too expensive
        5. "I need to talk to my manager/team"
        6. I'm the wrong person
        7. We tried something like this before
        8. You don't have the features we need
        9. Not Interested

# Day 3

# Outbound Prospecting & Email Overview
    How to Prospect via Email:
        1. Timing & Frequency
            - Time of day & which day (*BEST OPEN RATES FOR EMAILS IS ON THE WEEKENDS, but replies are very low)
        2. Optimize Wording
            - How to phrase the subject line
            - *What is the GOAL of the subject? Is it to OPEN the email or to get the MEETING
        3. Reason for Email
            - Clearly communicate WHY you are emailing
        4. Email Templates
            - Each serve a different purpose
    Fishing w/ Nets vs Spears
        - Mass Emails: Obviously a "spam" with low email replies
        - Semi-Personalized
        - Personalized: Significantly increases the chance for a reply
    Keep in mind:
        - Why are you emailing THEM?
        - Why are you emailing them TODAY?
    Subject lines are meant to get them to OPEN the email
    Subject line examples:
        - Congratulations on the [something in the press]
        - Question about [goal]
        - [Mutual connection] recommended to get in touch
        - [#] options to get started
        - [First name], [question]?
        - Should I stay or should I go?
        - Know [this] about [topic of interest]
        - Hoping to Help
        - A [benefit] for [company's name]
        - [Prospect's company name]//[your company's name]
        - [#] tips/ideas for [pain point]
        - Need less [pain point]?
        - Need more [value prop]?
        - [First name] - do you have 10 minutes for a conversation
        - Introduction: [prospect's name] <> [your name]
        - Idea for [topic the prospect cares about]
        - 10x [company]'s traction in 10 minutes
        - I found you through [referral's name]
        - [Referral's name] said you were in charge of [department]
        - 10 min - [date]?  
        - Re:
        - [Blank]
# - 3 parts to an Outbound email
        1. Why are you emailing THEM?
        2. What's in it for THEM? (Why should they care?)
        3. What are you asking for?
    Example of Good Outbound Email
        Subject: REI // Boomtrain
        
        Hi Jill,

        I recently read a Wall Street Journal article that mentioned your CMO is passionate about using machine learning to decrease online shopping cart abandonment rates. (REASON)

        I'm reaching out to you because Boomtrain works with major online retailers such as The North Face, Big 5, and Cabela's to cut shopping cart abandonment rates by up to 28% through our personalized UI layout. (WHY YOU SHOULD CARE)

        Do you have a few minutes next Tuesday at 11am or 4pm ET for me to share more details about the results we drive for major online retailers such as The North Face? (CALL TO ACTION)

        James
        Account Executive, Boomtrain

# - Email Templates
    Personal "Referral-From" Email
        Subject: Just spoke with Sally Mills

        Hello Mary,

        Sally Mills from your Human Resource team mentioned you were in charge of employee engagement and retention.

        I'm reaching out because I saw Sally's tweet this week about Cloudflare's initiative around improving employee engagement while remote. My company, Glint, helps large public companies such as Cloudflare improve employee engagement by 3x and improve employee retention by up to 40%. I'm curious if we could help Cloudflare achieve similar results.

        Can I send you an example of how we did this at Medallia who also went public in 2019?

        Best regards,

        Heather
        Sales Development, Glint (a Linkedln company)
    
