# Cloud Computing

# - Cloud Computing Fundamentals
    According to National Institute of Standards & Technology, Cloud is:
    1. On-Demand Self-Service: "... can provision capabilities as needed WITHOUT HUMAN INTERACTION"
    2. Broad Network Access: "Capabilities are available over the network and accessed through standard mechanisms..."
    3. Resource Pooling: "There is no sense of location independence... no control or knowledge over the exact location of the resources" and "... Resources are pooled to serve multiple consumers using a multi-tenant model"
    4. Rapid Elasticity: "Capabilities can be elastically provisioned and released to scale rapidly and outward and inward with demand." and "To the consumer, the capabilities available for provisioning often appear to be unlimited"
        - Can increase and decrease in size with relative ease in accordance with the current situation. ie Amazon can easily handle holiday sales, or Netflix handles the load generated from the latest episode of Game of Thrones.
        - SCALE IN SIZE ACCORDING TO LOAD
    5. Measuring Service: "Resource usage can be MONITORED, CONTROLLED, REPORTED, ... AND BILLED"
    Summation:
        1. On-Demand Self-Service
            - Provision & Terminate without human interaction
        2. Broad Network Access
            - Access over any networks and on any device
        3. Resource Pooling
            - Economies of Scale, Cheaper Service
        4. Rapid Elasticity
            - Scale UP & DOWN automatically in response to System Load
        5. Measured Service
            - Usage measured & pay what you use

# - Public vs Private vs Hybrid vs Multi Cloud
    1. Public Cloud: Must follow the 5 Cloud Fundamentals & Be available to the Public ie AWS, Microsoft Azure, Google Cloud
    2. Multi Cloud: Uses multiple Public Cloud platforms ie Uses both Azure & AWS
        - Provides "Cloud Provider Resilience", if one provider fails then at least part of your system remains functional
    3. Private Cloud: Follows the 5 Cloud Fundamentals, but is On-Premises and dedicated to you and your business ie AWS Outposts, Microsoft Azure Stack, Google Anthos
        - Different from Legacy On-Premises equipment ie VMware, Hyper-V, XenServer 
    4. Hybrid Cloud: Uses both Private & Public Cloud "cooperating together as a single environment"
        - Not Hybrid Environment (Connecting Public Cloud w On-Premises equipment)
    Summation:
        1. Public Cloud: Uses 1 Public Cloud
        2. Multi Cloud: Uses more than 1 Public Clouds
        3. Private Cloud: Uses On-Premises *real* Cloud
        4. Hybrid Cloud: Uses Public & Private Clouds
            - Hybrid is NOT Public Cloud + legacy On-Premises

# - Cloud Service Models
    - Part of the Stack is Managed by YOU or Managed by the VENDOR
    - Units of Consumption
    Infrastructure Stack:
        1. Facilities (Building)
        2. Infrastructure (Storage Networking)
        3. (Physical) Servers
        4. Virtualization (Carved up into Virtual Machines)
        5. O/S
        6. Container (ie Docker)
        7. Runtime (Coding language operates within a Runtime Environment)
        8. Data
        9. Application
    On-Premises System:
        - Must purchase all parts of the Infrastructure Stack
        - Expensive, Carries some Risks, but Very Flexible for your Business
    Data Center (DC) Hosting:
        - The VENDOR controls & owns the FACILITIES. YOU control the REST of the Infrastructure Stack
    Infrastructure As A Service (IaaS):
        - The Vendor manages Facilities, Infrastructure, Servers, and Virtualization. You CONSUME the O/S and manage the rest (Container, Runtime, Data, Application)
        - You PAY per time usage for the Virtual Machine
        - Popular Service Model
        - Substantial Cost & Risk reductions, but lose a bit of Flexibility
    Platform As A Service (PaaS):
        - Your UNIT of CONSUMPTION is RUNTIME environment and everything after that. The Vendor manages from the Facilities to the Container.
        - Mainly used by developers
    Software As A Service (Saas):
        - You CONSUME the APPLICATION ie Netflix, Hulu, CrunchyRoll, Dropbox, Office 365, Flickr, Google Mail
        - Almost no Risks or Costs. Standard, known services. No control over anything but the Application


# Tech Fundamentals

# - YAML 101 (Y'all Ain't Markup Language)
    YAML: human readable data serialization language
        - Language for defining data configuration that is meant to be human readable
        - supports numbers (1, 2, 3, etc)
        - supports floating point -> decimals (1.336)
        - supports boolean (true or false)
        - supports null (nothing)
    YAML key:value
        - A YAML document is *UNORDERED collection of "key: value" pairs, each key has a value
            - key: value example   cat1: roffle
    YAML List examples:
        'inline'
        adrianscats: ["roffle", "truffles", "penny", "winkie"]
        OR
        adrianscats:
          - "roffle"
          - "truffles"
          - 'penny'
          - winkie
        ^ the SAME Indentation SPACE = SAME List
    YAML Structure example:
        adrianscats:
            - name: roffle
              color: [black, white]
            - name: truffles
              color: "mixed"
            - name: penny
              color: "grey"
            - name: winkie
              color: "white"
              numofeyes: 1
    YAML Cloudformation specific example:
        Resources:
          s3bucket:
            Type: "AWS::S3::Bucket"
            Properties:
              BucketName: "ac137catpics"

# - JSON101 - JavaScript Object Notation
    JSON Object = unordered set of key:value pairs enclosed by {&}
    Object example:
        {"roffle": "cat", "sparky": "dog"}
    JSON Array = ordered collection of values, seperated by commas & enclosed in [&]
    Array example:
        [ "cat", "cat", "chicken", "cat"]
    JSON Array within Object list
        {
          "cats": ["roffle", "truffles", "penny", "winkie"],
          "colors": ["mixed", "mixed", "grey", "white"], 
          "numofeyes": ["2", "2", "2", "1"]
        }
    JSON Document (JSON Object within JSON Object)
        {
          "roffle" : {
            "color" : "mixed",
            "numofeyes": "2"
          },
          "truffles" : {
            "color": "mixed",
            "numofeyes": "2"
          },
          "penny" : {
            "color": "grey",
            "numofeyes": "2"
          },
          "winkie": {
            "color": "white",
            "numofeyes": "1"
          }
        } 
    JSON Cloudformation
        {
          "Resources": {
            "s3bucket": {
              "Type": "AWS::S3::Bucket",
              "Properties": {
                "BucketName": "ac1337catpics"
              }
            }
          }
        }

# - Network Starter Pack 
    OSI 7-Layer Model (Networking stack)
    7. Application
    6. Presentation
    5. Session
    4. Transport
    3. Network
    2. Data Link
    1. Physical
      - Layers 7 through 4 are HOST layers
      - Layers 3 through 1 are MEDIA layers
    
# - Layer 1 of OSI Model - Physical
    - *Every Layer above 1 has the capabilities of its preceding layers ie Layer 3 has the capabilities of 1-3
    - Physical Shared Medium
    - Standards for transmitting onto & receiving from the medium
    - No access Control
    - No uniquely identified devices
    - No device to device communications (everything broadcasted onto the shared physical medium)
    - Extra:
        Layer 1 (Physical) specifications define the transmission and reception of RAW BIT STREAMS between a device and a SHARED physical medium. It defines voltage levels, timing, rates, distances, modulation, and connectors
        Physical Medium may be Copper (electrical), Fibre (light), or WIFI (RF)
        Communicates in Binary via Voltage. Volt +1 = Binary 1; Volt -1 = Binary 0

# - Layer 2 - Data link
    Summation:
    - Identifiable devices
    - Media access control (sharing)
    - Collision Detection
    - Unicast .. 1:1
    - Broadcast 1:ALL
    - Switches (Overpowered Hubs)
    FRAME (Format of Sending info over Layer 2):
      - PREAMBLE (56 bits): Start of the FRAME
      - MAC HEADER: Destination Mac Address, Source Mac Adress, & EtherType(16 bits) -> controls frame destination, indicate source, & specify function
      - PAYLOAD: The DATA the FRAME is sending (46-1500 BYTES)
      - FRAME CHECK SEQUENCE (32 bits): End of the FRAME & Checks for any errors
    Layer 2 requires a functional Layer 1 to operate
    Devices @ L2 have a unique hardware address eg 3e:22:fb:b9:5b:75
    CSMA/CD (Carrier Sense Multiple Access)
      1. Game Alpha wants to send data to other Game Beta's hardware address. Layer 2 creates Frame1 (Encapsulates Data)
      2. CSMA checks the Carrier
      3. If no Carrier, Layer 1 transmits the Frame
      4. Layer 1 passes Frame to Layer 2
      5. But Game data needs to send data AT THE SAME TIME. Creates Frame2
      6. CSMA checks Carrier. Carrier DETECTED (Frame1 is being transmitted). Layer 2 WAITS until Carrier no longer detected to prevent collision
      7. Carrier no longer detected. Frame 2 sent to Layer 1 for transmission
        - When the Frames are receieved, Data is de-encapsulated (Payload extracted from Frame)
      8. If collisions are detected (both transmitting at once) both backoff for a time + random. Increases if another close collision.

# - Layer 3 - Network
    Summation:
        - IP Addresses (IPv4/6): cross network addressing
        - ARP: Finds the MAC address for this IP
        - Route: Where to forward packet to
        - Route Table: Multiple routes
        - Router: Moves packets from SRC (source) to DST (destination) - Encapsulating in L2 along the way
        - Device to Device Communication over the Internet
        - No method for channels of communication. SRC IP to DST IP
        - *PACKETS CAN BE DELIVERED OUT OF ORDER

    IP (Internet Protocol):
      - Layer 3 protocol that adds cross-network IP addressing & routing to move data between Local Area Networks without direct P2P links (point to point)
      - IP packets are moved step by step from SOURCE to DESTINATION via INTERMEDIATE networks - while encapsulated in different frames along the way
    Routers (L3 device):
      - removes frame encapsulation & adds new frame encapsulation @ every hop
    IP Packets:
      - Source IP address & Destination IP address (small on v4 packets, but massive on v6)
      - Protocol & Time to live (v4 only)
      - Hop limit (v6 only)
      - Data (from L4)
    
    IP Addressing (v4)
        - Uses Dotted-Decimal Notation 4 x #'s(0-255)   ie    133.33.33.7        10000101.00100001.00100001.00000111
          - *The beginning "133.33" is the 'Network' part representing what network it belongs to
          -  The ending "33.7" is the 'Host' part representing what a device on that network  ie  phone, laptop, etc
        - IP addresses are truly represented via binary = 10000101.00100001.00100001.00000111
          - 4 sets of 8 bits (eg "10000101") thus 32 bits
        - (This example in particular is a "/16" meaning the first 16 bits is the Network^ while the rest are Host)

    Subnet Mask:
      - Allows HOST to determine if an IP address it needs to communicate with local or remote - influences if a gateway is needed or can communicate locally
      - A Subnet Mask is configured on a host device in addition to an IP address eg 255.255.0.0 & this is the same as a /16 prefix
      - A subnet mask is a dotted decimal version of a binary number which indicates which part of an IP address is NETWORK (1) and which part is HOST (0)

<img src=”/img/'L3 Route and Route Table.png'”>

    Router & Route Table:
        1. HOME -> Packet created:
                Packet SRC 1.3.3.7 (Home). Destination 52.217.13.37 (this is the AWS IP address)
                Default route 0.0.0.0/0 sends all packets to ISP
           Router sends packet to your Internet Service Provider (ISP eg ATNT)
        2. ISP Router receives Packet
           Router has multiple interfaces for route table is used for selection
        3. Route Table eg:

                Destination         Next Hop/Target
               -52.217.13.0/24     -52.217.13.1
               -0.0.0.0/0          -52.42.214.1
               -52.43.214.0/24     -52.43.214.1

           Router compares packet Destination IP & Route Table for matching destinations. The more specfic prefixes are preferred (0 lowest,32 highest). Packet is forwarded on to the Next Hop/Target
        4. Packets are routed, hop by hop across the internet. From Source to Destination.
           Thus your packet hops from target to target until it reaches the AWS IP address.

    IP Routing
            - Can be akin to sending a gift, except the gift is opened at the (several) postage office(s) along the way - opened - checked to see if it belongs to this area - then repackaged and shipped onto the next and repeated until it reaches it's proper destination
        If the receiving end isn't local:
        1. Subnet Mask & Destination IP show that the other Laptop (receiver) isn't local.
        2. ARP finds the MAC address of the default GW (gateway), which is Router1.
        3. Packet with destination (eg 119.18.35.60) Encapsulated in Frame1. Laptop sends it to its Destination of Router1 MAC.
        4. Router1 receives Packet, removes Frame1, reviews packet IP destination.
        5. Router1 encapsulates Packet in Frame2 with Router2 as Destination MAC.
        6. Packet w/in Frame2 sent to Router2 Mac Address.
        7. Router2 receives Packet, removes Frame2
        8. Router2 confirms destination IP is on this same network. ARP gets MAC address of receiving Laptop. Packet is encapsulated in Frame3 with Destination MAC.
        9. Packet w/in Frame 3 is sent to the receiving laptop, its Final Destination.
            - Steps 4-6 repeated w/ a myriad of Routers and Networks until the Packet hops to its Final Destination

    Layer 3 Problems:
        - Packets can be lost en route
        - Different routes may result in out-of-order packets at the destination. No ordering mechanism
        - Different delays for different packets
        - No communications channels - packets have SRC & DST but no method of splitting APP or CHANNEL
        - No flow control. Packet loss may ensue from oversaturation

# - Layer 4 - Transport, (feat. L5)
        - TCP = Slower but Reliable
        - UDP = Faster but Less Reliable
        - Uses IP as transit
        FOCUSING ON TCP

    TCP Segments
        - TCP segments encapsulated in IP packets
        - Segments dont have SRC or DST IP's - packets provide addressing 
      Parts:
        - Source Port
        - Destination Port
        - Sequence #
        - Acknowledgement
        - Flags N Things
        - Window (Controls data size)
        - Checksum
        - Urgent Pointer
        - Options & Padding
        - Data

    Transmission Control Protocol (TCP)
        Communication between two devices using a random port on a Client & known port on the Server
      - Segments linked in connection, error checking, ordering, and retransmission
      - *TCP GIVES RELIABLE ORDERED SEGMENTS
            Ephemeral Ports (High)
            Well Known Ports

    TCP Connection 3-way Handshake
        Flags N Things
            - FIN = close
            - ACK = acknowledgements
            - SYN = synchronise sequence numbers
      1. SYN - send segment with SYN sequence set 'cs' Random value
      2. SYN-ACK - Increment 'cs' (=cs+1), Picks random sequence 'ss', Sends Segment = SYN-ACK, Sequence set to 'ss', Acknowledgement set to 'cs+1' 
      3. ACK - Increment ss (ss+1), Increment cs+1 (=cs+2), Send segment with ACK, Sequence set to cs+2, Acknowlesge set to ss+1
      4. Connection established, Client can send data

    Sessions & State
        - Ephemeral Port eg tcp/23060
        - Well Known Port eg tcp/443
      Stateless Firewall (eg Network ACL AWS) sees:
        - Outbound (OUT) eg Laptop-IP & tcp/23060 -> Server-IP & tcp/443
        - Response (IN) eg  Server-IP & tcp/443 ->  Laptop-IP & tcp/23060
      Stateful Firewall
        - Outbound eg Laptop-IP & tcp/23060 -> Server-IP & tcp/443
        Allowing the outbound implicitly allows the inbound response

# - Network Address Translation (NAT)
    - IPv4 ONLY
    - NAT is designed to overcome IPv4 shortages
    - security benefits
    - Translates Private IPv4 addresses to Public
    - STATIC NAT: 1 private to 1 (fixed) public address
    - DYNAMIC NAT: 1 private to 1st available public address
    - PORT ADDRESS TRANSLATION (PAT): many private to 1 public

    Static Network Address Translation
        - AWS, IGW (Internet Gateway)
        - NAT Table -> PrivateIP:PublicIP (1:1)
      1. Packets are generated as normal with private SRC & extrnal DST IP (eg SRC IP:10.0.0.42, DST IP:NETFLIX)
      2. Public IP allocated to Private IP. By passing through the NAT device, source address is translated
      3. API Client communicates with eg Cat API. Packets arrive with a SRC of 52.95.36.68
      4. Cat API. WebService. IP: 1.3.3.7
      5. SRC IP: CatAPI. DST IP: 52.95.36.68
      6. Inbound packets are checkd, if a matching entry is found in NAT Table, the DST Public IP is translated to the matching Private IP

    Dynamic Network Address Translation
        - (NAT Table PrivateIP:PublicIP)
        - Public IP allocations are temporary allocations from a Public IP Pool
      1. SRC IP: 10.0.0.10. DST IP: 1.3.3.7. Passes through NAT device
      2. 10.0.0.10 is dynamically allocated 52.95.36.67
        - Public Allocations are temporary and multiple devices can use the same allocation over time as long as there is no overlap
        - Only one Private IP will be mapped to 52.95.36.66 at any time. It's still 1:1 for the duration of the allocation
        - If the Public IP Pool is exhauseted ... external access can fall

    Port Address Translation
        - In AWS this is how the NATGateway functions - a (MANY:1) (PrivateIP:PublicIP) Architecture
        - NAT Device records the SRC (Private) IP & SRC Port. It replaces the SRC IP with the single Public IP and a public source port allocated from a pool which allows IP Overloading (MANY:1)
      1. Laptop1 SRC IP: 10.0.0.42. SRC PORT: 32678. DST IP: 1.3.3.7. DST PORT: 443. Sent to NAT Public IP
      2. SRC IP: 52.95.36.67. SRC PORT:1337. DST IP: 1.3.3.7. DST PORT: 443
      3. Laptop2 SRC IP: 10.0.0.43. SRC PORT: 32769. DST IP: 1.3.3.7. DST PORT: 443.
      4. SRC IP: 52.95.36.67. SRC PORT: 1338. DST IP: 1.3.3.7. DST PORT: 443
      5. Laptop3 SRC: 10.0.0.44. SRC PORT: 32768. DST IP: 1.3.3.7. DST PORT: 443
      6. SRC IP: 52.95.36.67. SRC PORT: 1339. DST IP:1.3.3.7. DST PORT: 443
        - Return traffic has tcp/443 and 1.3.3.7 as the source and the Public IP of the NAT deivce and the destination port
        - Public Port and Public IP are translated to Private Port and Private IP

# - IP Addressing & Subnetting
    IPv4 Addressing & Subnetting
        - 0.0.0.0 -> 255.255.255.255, 4,294,967,296 IPv4 Addresses
        - All public IPv4 addressing is allocated
        - Defined by a standard RFC 1918

      Class A. Start 0.0.0.0 -> End 127.255.255.255
        - Huge Businesses Early Internet and some military 
        - 128 Networks, 16.777,216 IPs Each
      Class B. Start 128.0.0.0 -> End 191.255.255.255
        - 16,384 Networks, 65,536 IPs each
        - Large Businesses that don't require Class A
      Class C. Start 192.0.0.0 -> End 233.255.255.255
        - 2,097,152 Networks, 256 IPs each
        - Smaller Businesses, not enough for Class B
      Class D. for Multicast
      Class E. for Reserved

        - 10.0.0.0-10.255.255 (1 x Class A Network)
        - .. 16.777,216 IPv4 addresses
        - 172.16.0.0-172.31.255.255 (16 x Class B Networks)
        - .. 16 x 65,536 IPv4 Addresses
        - 192.168.0.0-192.168.255.255 (256 x Class C Networks)
        - .. 256 x 256 IPv4 Addresses

    IPv4 Address Space
        - Far too few IPv4 spaces
        - IPv6 space is 430,282,366,920,938,463,463,374,607,431,770,000,000 (50 octillion per human alive)

# - DDoS
    - Attacks designed to overload websites
    - Compete against legitimate connections
    - Distributed - hard to block indiviudal IPs/Ranges
    - APPLICATION Layer: HTTP Flood
    - PROTOCOL Attack: SYN Flood
    - VOLUMETRIC: DNS Amplification
    - .. often involve large armies of compromised machines (botnets)

# - SSL and TLS
        - Privacy and Data Integrity between client and server
        - Privacy - communcations encrypted
        - ... asymmetric and then symmetric encryption
        - Identity (server or client/server) verified
        - Reliable connection - protection against alteration of data

    Three Phases:
    1. Cipher Suites
        - Asymmetric
        - TLS begins with an established TCP connection. Agree the method of communications, the "Cipher suite"
            A. Client HELLO
            B. Server Hello
                - At this point, the client and server have agreed to communicate and the client has the Server Certificate, which contains the server 'Public Key'
                - Publc trusted Certficate Authority (CA). Server generates a pub/private key pair and a Certificate Signing request (CSR) the public CA generates a signed certificate
    2. Authentication
        - Ensure the Server Certificate is authentic
        - Public trusted CA
            C. The client trusts the public CA - it makes sure the certificate is valid (by date and isn't revoked) and that the DNS name matches the name on the cert
            D. The client verifies the server has the private key
    3. Key Exchange
        - Move from Asymmetric to Symmetric keys in a secure way and begins encryption process
            E. The client generates the pre-master key, encrypts it with the servers public key and sends it to the server
            F. The server decrypts the pre-master key using its private key
            G. Both sides use the same pre-master key to generate the master secret which is used to generate the ongoing session keys which encrypt and decrypt data
            H. Both sides confirm the handshake and from them on, communications between client to server are encrypted


# AWS Fundamentals

# - AWS Public vs Private Services
    All about networking ONLY ... no permissions by default
    Three Zones:
    A. Public Internet
    B. AWS Public
        - S3 Bucket (Public Service)

        - *ANYONE CAN CONNECT, BUT PERMISSIONS ARE REQUIRED TO ACCESS THE SERVICE

    C. AWS Private
        - By default, no permissions of connection from anything. Isolated by default
        - Divided by VPC, into two parts
            - EC2 Instance (no connection to internet, isolated), but possible for *outgoing connection OR connect to AWS Public then connect into Public Internet. On-premises network may connect to EC2 Instance

            - *LOCATED IN VPC, ACCESSIBLE FROM THE VPC IT IS LOCATED IN, ACCESSIBLE FROM OTHER VPCs AS LONG IT IS CONFIGURED TO THEM

# - AWS Global Infrastructure
    - Geographic Separation - Isolated Fault Domain
    - GeoPOLITICAL Separation
    - Location Control - Increased Performance
    - Globally Resilient 
    - Region Resilient 
    - AZ Resilient (Availability Zone) 
    - Multiple AZ's within each Region

# - AWS Default Virtual Private Cloud
    VPC Basics:
      - A VPC = A Virtual Network Inside AWS
      - A VPC is within 1 account & 1 region
      - Private and Isolated unless you decide otherwise
      - Two types: Default and Custom VPCs
      - 1 Default per Region, Multiple Custom per Region
      - Custom VPC by default is private
      - VPC CIDR 172.31.0.0/16 defines that Start and Ending range of IP addresses that VPC can use
        - Subnets make pieces within the VPC



        SUBNETS BY DEFAULT ARE EQUAL TO THE NUMBER OF AZs IN THE REGION OF THE VPC



    Default VPC Facts
        - One per region - can be removed & recreated
        - Default VPC CIDR is always 172.31.0.0/16
        - /20 Subnet in each AZ in the region
        - Internet Gateway (IGW), Security Group (SG), & NACL
        - Subnets assign public IPv4 addresses

# - Elastic Compute Cloud (EC2)
    Key features:
        - IAAS - Provides Virtual Machines 
        - Private service by-default - uses VPC networking
        - AZ Resilient - Instance fails if AZ fails
        - Different instance sizes and capabilities
        - On-Demand Billing - Per second
        - Local on-host sotrage or Elastic Block Store (EBS)

    Instance Lifecycle
        Running
            Uses CPU
        Stopped
            No CPU or memory consumed nor network data
        Terminated (Non-reversible)

    Amazon Machine Image
        - Can create or be created from EC2
        - AMI uses Permissions:
            - Public Permission - everyone allowed
            - Owner - Implicit Allowed
            - Explicit - specifc AWS accounts allowed
        - AMI uses Root Volume:
        - AMI uses Block Device Mapping


    *WHAT IS STORED ON AN AMI
        - Boot Volume
        - Data Volume
        - AMI permissions
        - Block Device Mapping
    *What is NOT ON AN AMI
        - instance settings
        - network settings


    Connecting to EC2
        On Windows: Private and Public Key

# - Simple Storage Service (S3)


        - *S3 IS AN AWS PUBLIC SERVICE, AN OBJECT STORAGE SYSTEM, & THEIR BUCKETS HAVE UNLIMITED SPACE



        - Global Storage Platform - Regional based/resilient
        - Public service, unlimited data & multi-user
        - ... Movies, Audio, Photos, Text, Large Data sets
        - Economical & accessed via UI/CLI/API/HTTP
        - Objects & Buckets (Contains Objects)
    Objects
        - Key (Identity) eg koala.jpg
        - Value: Content being stored
            - Range of Value: 0 bytes to 5TB
    Buckets
        - Primary home region, unless you change it
        - Bucket name
            - Must be totally unique
            - Can hold unlimited objects (infinitely scalable system)
        - Flat Structure (no files within folders within folders)
        - But can present folders by writing prefixes eg /'inserttitle'/name.jpg

    *Summation:
        - Bucket names:
            - GLOBALLY UNIQUE
            - 3-63 characters
            - all lower case
            - no underscores
            - start with lowercase letter or number
            - Can't be IP formatted eg 1.1.1.1
        - Bucket limit:
            - 100 Soft limit, 1000 hard limit per account
            - Unlimited objects in bukcet, 0 bytes to 5 TB
        - Key: name
        - Value: data

    S3 Patterns & Anti-Patterns
        - S3 is an object store - not file or block
        - You CAN'T MOUNT an S3 Bucket as (K:\ or /images)
        - Great for large scale data storage, distribution, or upload
        - Great for 'offload'
        - INPUT and/or OUTPUT to MANY AWS products

# - Cloud Formation (CFN) Basics 
        CFN composed of Templates
            - Templates written in either  YAML or JSON
            - All Templates written with "Resources", its Mandatory
            - *Description must directly follow the template format version
          Resources:



            - *LOGICAL RESOURCES ARE DEFINED IN A CFN TEMPLATE
            - *PHYSICAL RESOURCES ARE MADE BY CREATING A CFN STACK



          One Template can create multiple Stacks
            - Creates a Physical Resource. ie EC2 Instance
            - Able to Update Stacks, thus affects Physical Resources
            - Able to Delete Stacks, thus deletes Physical Resources

# - CloudWatch Basics
        Basics:
            - Collects and manages operational data
            - METRICS on AWS Products, Apps, on-premises (monitoring data)
            - CloudWatch Logs on AWS Products, Apps, on-premises
            - CloudWatch Events - AWS Services & Schedules

# - Shared Responisibility Model
    AWS is responsible for the Security of the Cloud:
        - Software
        - Compute, Storage, Database, Networking
        - Hardware/Global Infrastructure
        - Regions, AZ, Edge Locations
    Customer is responsible for the Secruity IN the Cloud:
        - Customer Data
        - Platform, Applications, Identity, & Access Management
        - Operating System, Network, & Firewall Config.
        - Client-side Data Encryption, Integrity & Authent.; Server-Side Encryption (File System and Data); Networking Traffic Protection

# - High-Availability vs Fault-Tolerance vs Disaster Recovery
    High-Availability: TAG-IN
        - aims to ensure an agreed level of operational performance, usually uptime, for a higher than normal period.
        - HA switches out the entire server during an outtage or constantly has an extra server on standby to switch-in in-case an outtage occurs
            - May cause user disruption eg have to log-in back in again
        Summation:
            - HA, is when you have a "spare tire" (server) in-case "tire pop" (outtage)

    Fault-Tolerance: TANK HITS
        - is the property that enables a system to continue operating properly in the event of the failure of some (one or more faults within) of its components
        - OPERATE THROUGH FAILURE
            - Multiple servers so to be ABLE TO TAKE A HIT
        - eg hospitals have anesthesia distribution systems so it cannot handle any disruptions, so it has mutiple servers to multiple monitors
        Summation:
            - FT, is a plane's duplicate electronic & hydraulic systems capable of running even if one or two is down

    Disaster-Recovery: BACKUP BODY
        - a set of policies, tools, and procedures to enable the recovery or continuation of vital technology infrastructure and systems following a natural or human-induced disaster
        - OFF-SITE BACKUP platform, storage, server, location (premises), EVERYTHING
        - Used when HA and FT don't work
        Summation:
            - DR, Funny Valentine or D&D 5e Clone spell

    HA, FT, DR Summation:
        - HA: Minimise any outages eg Spare Tire
        - FT: Operate Through Faults eg Tank the Hits
        - DR: Used when HA and FT don't work eg 5e Clone Spell

# - Domain Name System (DNS)
    101
        - DNS is a discovery machine
        - Translates machine into human & vice-versa
        - amazon.com -> 104.98.34.131
        - Huge and must be distributed
        - 4,294,967,296 IPv4 Addresses, AND ALOT MORE IPv6
        - DNS finds name server (holds zone file), retrieve info and pass it back to DNS client

    Remember These!
        - DNS Client: Laptop, Phone, Tablet, PC
        - Resolver: Software on your device, or a server which queries DNS on your behalf
        - Zone: part of the DNS Database
        - Zonefile: Physical database for a zone
        - Nameserver: where zonefiles are hosted

    DNS Root
        1. DNS asks IP for given DNS name
        2. DNS resolver communicates with root server(s) to access root zone, begin process of finding IP address
        - Authority - IANA

    REMEMBER THESE
        - Root Hints: config points at the root servers IPs and addresses
            - asks HERE first^
        - Root Server: hosts DNS root zone
        - Root Zone: points at TLD authoritative servers
            - next step in the hierarchy
        - gTLD: generic top level domain (.com .org)
        - ccTLD: country-code top level domain (.uk .eu etc)

        12 LARGE ORGANISATIONS MANAGE DNS ROOT SERVERS
        THERE ARE 13 ROOT SERVERS
        IANA MANAGES DNS ROOT ZONE

        DNS RECORD TYPE: "A" CONVERTS A 'HOST' INTO AN IPv4 ADDRESS
        DNS RECORD TYPE: "NS" DELEGATES CONTROL OF .org TO THE .org REGISTRY

        THE REGISTRY ORGANIZATION MAINTAINS ZONES FOR A TLD
        THE REGISTRAR ORGANIZATION ALLOWS DOMAIN REGISTRATION

# - Route53
    Basics:
        - R53 Register Domains
        - R53 Host Zones.. manage nameservers
        - Globl Service.. single database
        - Globally Resilient

    Hosted Zones:
        - Zone files in AWS
        - Hosted on 4 managed name servers
        - Can be public..
        - Or private.. linked to VPC(s)



# IAM, ACCOUNTS, AND AWS ORGANIZATIONS

# - IAM Identity Policies
    IAM Policy Documents written in JSON
        - compromised of Statements
        eg:

        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Sid": "FullAccess",
                    "Effect": "Allow",
                    "Action": ["53:"*),
                    "Resource": ["*"]
                },
                {
                    "Sid": "DenyCatBucket",
                    "Action": ["53:*"),
                    "Effect": "Deny",
                    "Resource": ["arn:aws:53::: catgifs", "arn:aws:53::: catgifs/*" ]
                }
            ]
        }

    -  This: 
                    "Sid": "FullAccess",
                    "Effect": "Allow",
                    "Action": ["53:*"),
                    "Resource": ["*"]
      part allows full access of all of S3 service. AKA *EXPLICIT ALLOW


    - This:
                    "Sid": "DenyCatBucket",
                    "Action": ["53:*"),
                    "Effect": "Deny",
                    "Resource": ["arn:aws:53::: catgifs", "arn:aws:53::: catgifs/*" ]
      part denies access to a small portion of S3 -> that being the catgifs bucket. AKA *EXPLICIT DENY
    - EXPLICIT DENYs ALWAYS TAKE PRIORITY OVER EXPLICIT ALLOWs
    - Default DENY (Implicit) automatically takes effect if neither Explicit Deny or Allow 

    RULE OF THUMB in priority:
        1. Explicit DENY
        2. Explicit ALLOW
        3 Default DENY (Implicit)

    IAM Policies
    - The multiple policies are all evauluated SIMULTANEOUSLY but Rule of Thumb still applies
    - Managed Policy:
        - Reusable (Used for Small, large numbers of people) (Used for common access rights given to your average member)
        - Low Management Overhead 
    - Inline Policy:
        - For special or exceptional Allow or Denys

# - IAM Users and ARN
    IAM Users:
        - an identity used for anything requiring long-term AWS access eg Humans, Applications or service accounts
        - Principal:
            - an individual, computer, service, or a group of any of these
    Authentication:
        - proves your identity via Username/Password or Access Keys
    Authorization:
        - IAM checks authentication and allows/denies access
    Amazon Resource Name (ARN):
        - Uniquely identify resoiurces within any AWS accounts
        - used in IAM policies
        - eg

            arn:partition:service:region:account-id:resource-id
            arn:partition:service:region:account-id:resource-type/resource-id
            arn:partition:service:region:account-id:resource-type: resource-id

            arn:aws:53:::catgifs
            arn:aws:53:::catgifs/*

        - This part:
            arn:aws:53:::catgifs
          references the BUCKET itself

        - This part:
            arn:aws:53:::catgifs/*
          references the OBJECTS within the bucket

    IMPORTANT*
        - *******maximum of 5000 IAM Users per account
        - IAM User can be a member of a maximum of 10 groups
        - This has systems design impacts...
        - Internet-scale applications
        - Large orgs & orgs merges
        - IAM Roles & Identity Federation fix this 

